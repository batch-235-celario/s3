const chai = require("chai");
const { assert } = require("chai");

// import and use chai-http to allow chai to send requests to our server
const http = require("chai-http");
chai.use(http);

describe("API Test Suite for users", () => {

	it("Test API get users is running", (done) => {

		// request() method is used from chai to create an http request given to the server
		// get("/endpoint") method is used to run/ access a get method route
		// end() method is used to access the response from the route. It has anonymous function as an argument that receives 2 objects, the err or the response
		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			// isDefined is assertion that given data is not undefined. It's like a shortcut to .notEqual(typeof data, undefined)
			assert.isDefined(res);
			// console.log(res)

			// [SOLUTION for undefined response from the route]
			// assert.notEqual(typeof res.body.users, "undefined")
			// assert.notEqual(typeof res.text, "")

			// done() method is used to tell chai-http when the test is done
			done();
		})
	})

	it("Test API get users returns an array", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			// res.body contains the body of the response. The data sent from res.send()
			// isArray() is an assertion that the given data is an array
			console.log(res.body);
			assert.isArray(res.body);
			done();
		})
	})
	it("Test API get users array first object username is Jojo", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {

			assert.equal(res.body[0].username, "Jojo");
			done();
		})
	})

	/*
		Mini-Activity: 10 mins

		Create a test case to check/confirm the last item in the users array.

		Send your passing test output in Batch Hangouts

	*/

	it("Test API get users array last item is not undefined", (done) => {

		chai.request("http://localhost:4000")
		.get("/users")
		.end((err, res) => {
			assert.notEqual(res.body[res.body.length-1], undefined)
			done();
		})
	})

	it("Test API post users returns 400 if no name", (done) => {

		// post() which is used by chai http to access a post method route
		// type() which is used to tell chai that request body is going to be stringified as a json
		// send() is used to send the request body
		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			// name: "jin",
			age: 30,
			username: "jin92"
		})
		.end((err, res) => {
			console.log(res.status)
			assert.equal(res.status, 400)
			done();
		})

	})

	/*
		Mini-Activity: 10 mins
		Create another condition in the post /users route for when age is not given it should return a Bad Request with an error message

		Create a test case to check if there is no age, it should return a 400 status.

		Send your passing test output in Batch Hangouts

	*/

	it("Test API post users returns 400 if no age", (done) => {

		chai.request("http://localhost:4000")
		.post("/users")
		.type("json")
		.send({
			name: "jin",
			username: "jin92"
		})
		.end((err, res) => {
			console.log(res.status)
			assert.equal(res.status, 400)
			done();
		})

	})

})

// Activity

describe("Test for artists", () => {
	it("test if artists endpoint can recieve a response", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			assert.isDefined(res);
			done();
		})
	})

	it("test if songs property of first object is an array", (done) => {
		chai.request("http://localhost:4000")
		.get("/artists")
		.end((err, res) => {
			//console.log(res.body[0].songs);
			assert.isArray(res.body[0].songs)
			done();
		})
	})

	it("Check if the post endpoint encounters an error if there is no name", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			//name: "Arthur Nery",
			songs : ["Isa lang", "Pahinga"],
			album: "letters never sent",
			isActive: true
		})
		.end((err, res) => {
			//console.log(res.status)
			assert.equal(res.status, 400)
			done();
		})

	})

	it("Check if the post endpoint encounters an error if there is no songs", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "artist 1",
			//songs : ["song test 1", "song test 2"],
			album: "album 1",
			isActive: true
		})
		.end((err, res) => {
			//console.log(res.status)
			assert.equal(res.status, 400)
			done();
		})

	})

	it("Check if the post endpoint encounters an error if there is no album", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "artist 2",
			songs : ["song test 3", "song test 4"],
			//album: "album 2",
			isActive: true
		})
		.end((err, res) => {
			//console.log(res.status)
			assert.equal(res.status, 400)
			done();
		})

	})

	it("Check if the post endpoint encounters an error if the isActive property is equal to false", (done) => {

		chai.request("http://localhost:4000")
		.post("/artists")
		.type("json")
		.send({
			name: "Artist 3",
			songs : ["test", "test2"],
			album: "albumsssss",
			isActive: false
		})
		.end((err, res) => {
			//console.log(res.status)
			assert.equal(res.status, 400)
			done();
		})

	})

})